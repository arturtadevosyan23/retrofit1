package com.example.retrofitschema;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostsViewHolder> {
        private Context context;
        private PostsResponse postsResponses;

    public PostAdapter(Context context, PostsResponse postsResponses) {
        this.context = context;
        this.postsResponses = postsResponses;
    }

    @NonNull
    @Override
    public PostsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PostsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_posts,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull PostsViewHolder holder, int position) {
        holder.textUserID.setText(String.valueOf(postsResponses.getUserId()));
        holder.textID.setText(String.valueOf(postsResponses.getId()));
        holder.texTitle.setText(postsResponses.getTitle());
        holder.textBody.setText(postsResponses.getBody());

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    static class PostsViewHolder extends RecyclerView.ViewHolder {
                AppCompatTextView textID;
        private AppCompatTextView textUserID;
        private AppCompatTextView texTitle;
        private AppCompatTextView textBody;


        public PostsViewHolder(@NonNull View itemView) {
            super(itemView);
            textID = itemView.findViewById(R.id.txt_view_id);
            textUserID = itemView.findViewById(R.id.txt_view_user_id);
            texTitle = itemView.findViewById(R.id.txt_view_Title);
            textBody = itemView.findViewById(R.id.txt_view_body);
        }
    }
}
