package com.example.retrofitschema;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Api {
    @GET("users")
    Call<List<PersonFields>> users();

    @GET("posts/{id}")
    Call<PostsResponse> getPostID(@Path("id") int id);
}
