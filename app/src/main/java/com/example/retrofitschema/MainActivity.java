package com.example.retrofitschema;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.google.gson.internal.$Gson$Types;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    PersonFieldAdapter adapter;
    List<PersonFields> personFields = new ArrayList<>();
    Api api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.rec_view);
        connect();


        getPosts(new Random().nextInt(99)+1);

    }


    private void connect(){
        api = Client.clientInstance().create(Api.class);
    }

    void init(){

        Call<List<PersonFields>> call = api.users();
        call.enqueue(new Callback<List<PersonFields>>() {
            @Override
            public void onResponse(Call<List<PersonFields>> call, Response<List<PersonFields>> response) {

                recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                adapter = new PersonFieldAdapter(MainActivity.this,response.body());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<PersonFields>> call, Throwable t) {

            }
        });





    }

    void  getPosts(int id){

        Call<PostsResponse> call = api.getPostID(id);
        call.enqueue(new Callback<PostsResponse>() {
            @Override
            public void onResponse(Call<PostsResponse> call, Response<PostsResponse> response) {
                if (response.body()!=null){
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

                    recyclerView.setAdapter(new PostAdapter(MainActivity.this,response.body()));
                }
            }

            @Override
            public void onFailure(Call<PostsResponse> call, Throwable t) {

            }
        });
    }

}