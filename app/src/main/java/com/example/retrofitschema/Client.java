package com.example.retrofitschema;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Client {
 public static    Retrofit retrofit = null;

public static Retrofit clientInstance(){
    if (retrofit == null){
        return retrofit =new Retrofit.Builder().baseUrl(Constants.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    }else
    return retrofit;
}


}
