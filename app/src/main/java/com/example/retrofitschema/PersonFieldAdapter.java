package com.example.retrofitschema;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PersonFieldAdapter extends RecyclerView.Adapter<PersonFieldAdapter.PersonHolder> {
    Context context;
    List<PersonFields> personFields;

    public PersonFieldAdapter(Context context, List<PersonFields> personFields) {
        this.context = context;
        this.personFields = personFields;
    }

    @NonNull
    @Override
    public PersonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PersonHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.info,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull PersonHolder holder, int position) {
        holder.name.setText(personFields.get(position).getName());
        holder.userName.setText(personFields.get(position).getUsername());
        holder.email.setText(personFields.get(position).getEmail());
        holder.phone.setText(personFields.get(position).getPhone());
        holder.website.setText(personFields.get(position).getWebsite());
        holder.street.setText(personFields.get(position).getAddress().getStreet());
        holder.suite.setText(personFields.get(position).getAddress().getSuite());
        holder.city.setText(personFields.get(position).getAddress().getCity());
        holder.zipcode.setText(personFields.get(position).getAddress().getZipcode());
        holder.zipcode.setText(personFields.get(position).getAddress().getZipcode());
        holder.lat.setText(personFields.get(position).getAddress().getGeo().getLat());
        holder.lng.setText(personFields.get(position).getAddress().getGeo().getLng());
        holder.companiName.setText(personFields.get(position).getCompany().getName());
        holder.catchprase.setText(personFields.get(position).getCompany().getCatchPhrase());
        holder.bs.setText(personFields.get(position).getCompany().getBs());
        holder.id.setText(String.valueOf(personFields.get(position).getId()));


    }

    @Override
    public int getItemCount() {
        return personFields.size();
    }

    static class PersonHolder extends RecyclerView.ViewHolder {
        AppCompatTextView name,userName,email,phone,website,street,suite,city,zipcode,lat,lng,companiName,catchprase,bs,id;

        public PersonHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.txt_view_id);
            name = itemView.findViewById(R.id.txt_view_client_name);
            userName = itemView.findViewById(R.id.txt_view_client_user_name);
            email = itemView.findViewById(R.id.txt_view_client_email);
            phone = itemView.findViewById(R.id.txt_view_client_Phone);
            website = itemView.findViewById(R.id.txt_view_client_website);
            street = itemView.findViewById(R.id.txt_view_adress_street);
            suite = itemView.findViewById(R.id.txt_view_adress_suite);
            city = itemView.findViewById(R.id.txt_view_adress_city);
            zipcode = itemView.findViewById(R.id.txt_view_adress_zipcode);
            lat = itemView.findViewById(R.id.txt_view_adress_geo_lat);
            lng = itemView.findViewById(R.id.txt_view_adress_geo_lng);
            companiName = itemView.findViewById(R.id.company_name);
            catchprase = itemView.findViewById(R.id.company_catchprase);
            bs = itemView.findViewById(R.id.company_bs);

        }
    }
}
